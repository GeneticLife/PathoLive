PathoLive - Description
=======================================

Description
-----------

PathoLive is a real time pathogen diagnostics tool that detects pathogenic 
sequences from metagenomic Illumina sequencing data while they are
produced. This means, a hint towards a diagnosis is provided before the
sequencer is finished. 


Website
-------

The Name project website is https://gitlab.com/SimonHTausch/PathoLive

There you can find the latest version of PathoLive, source code, documentation,
and examples.


Installation
------------

If you are using a Debian based system you can directly install the Debian 
package from the official [Debian repository](https://packages.debian.org/sid/hilive "HiLive Debian package")

If this does not work for you, you can still compile PathoLive from source.

Make sure that the following dependencies are installed:

 * HiLive (>= v1.0)
 * python3 

 
Usage
-----


#### Default mode
To use PathoLive, please download and unpack all provided files without changing their structure.

To start PathoLive with the default settings, enter:

    ./PathoLive -i /path/to/BaseCalls -x /path/to/reference.kix -r /path/to/resultfolder -H /path/to/hilive
    
PathoLive will produce .SAM files for the specified cycles and produce a browser-accessible visualization in the file ``results.html`` in your result folder.

We recommend to adjust the numbers of threads used by PathoLive with -n. If possible,
make use of as many threads as tiles are produced by your sequencer.

Building your own reference database
------------------------------------

Disclaimer: This requires to download several hundreds of gigabytes of data. We  advise you to use the reference database provided by us.

Make sure to install the following additional dependencies:
 * bowtie2
 * fastq-dump
 * trimmomatic

Build a Bowtie2 index as well as a HiLive index of your reference file. 
Insert the paths to the Bowtie2 index and a result folder and start the bash-script 
    ``../background_definition/download_trim_map_background.bsh``.

Then start the python script ``calculate_overall_background_coverage.py`` with the provided result folder as input folder and replace the file ``prelim_data/background_coverages.pickle`` in your PathoLive-folder by the new one.


License
-------

See the file LICENSE for licensing information.


Contact
-------

Please consult the HiLive project website for questions!

If this does not help, please feel free to consult:

 * Simon H. Tausch <tauschs (at) rki.de> 

